<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('login',32)->index();
            $table->string('pswd',32)->index();
            $table->enum('role_type',[
                'Administrator',
                'Moderator',
                'Taxis',
                'Client',
            ]);
            $table->tinyInteger('status')->default(0);
            $table->string('token',64)->index();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
};

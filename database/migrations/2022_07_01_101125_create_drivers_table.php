<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('drivers', function (Blueprint $table) {
            $table->id();
            $table->string('car_num',16);/*davlat avtoraqam*/
            $table->string('tex_pasport_num',16);/*tex passport seriya*/
            $table->string('drivers_license_photo',255)->nullable(); /*prava*/
            $table->tinyInteger('status')->default(0); /*status agar 0 bo`lsa verifikatsiyadan otkazish kerak agar 1 bo`lsa active date tugaguncha verifikatsiyadan otgan hisoblanadi*/
            $table->timestamp('active_untill')->nullable();/*verifikatsiyadan otkanligi tagaydigan muddat*/
            $table->string('tex_pasport_photo',255)->nullable(); /*texpasport*/
           $table->string('model',32); /*avtomobil modeli*/
           $table->string('color',16); /*avtomobil rangi*/
           $table->tinyInteger('year'); /*avtomobil ishlab chiqarilgan yil*/
           $table->tinyInteger('passengers'); /*avtomobil orindiqlari soni*/
           $table->tinyInteger('has_bag'); /*bagaj mavjudmi*/


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('drivers');
    }
};

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();

            $table->integer('user_taxis_id')->index();
            $table->text('comment')->nullable();
            $table->enum('status',[
               'neworder',
               'agreed',
               'waitcar',
               'processed',
               'endorder',
               'cancelled',
            ]);
            $table->enum('payment_type',[
                'cash',
                'card',
            ]);
            $table->float('adress_untill_lat')->nullable();
            $table->float('adress_untill_long')->nullable();
            $table->integer('count_passengers');
            $table->integer('price_offer');
            $table->timestamp('order_time');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
};

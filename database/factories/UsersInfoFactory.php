<?php

namespace Database\Factories;

use App\Models\Users_info;
use Illuminate\Database\Eloquent\Factories\Factory;

class UsersInfoFactory extends Factory
{
    protected $model = Users_info::class;

    public function definition(): array
    {
    	return [
    	    //
    	];
    }
}

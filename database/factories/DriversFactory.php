<?php

namespace Database\Factories;

use App\Models\drivers;
use Illuminate\Database\Eloquent\Factories\Factory;

class DriversFactory extends Factory
{
    protected $model = drivers::class;

    public function definition(): array
    {
    	return [
    	    //
    	];
    }
}

<?php

namespace Database\Factories;

use App\Models\client_info;
use Illuminate\Database\Eloquent\Factories\Factory;

class ClientInfoFactory extends Factory
{
    protected $model = client_info::class;

    public function definition(): array
    {
    	return [
    	    //
    	];
    }
}

<?php

namespace Database\Factories;

use App\Models\chat;
use Illuminate\Database\Eloquent\Factories\Factory;

class ChatFactory extends Factory
{
    protected $model = chat::class;

    public function definition(): array
    {
    	return [
    	    //
    	];
    }
}

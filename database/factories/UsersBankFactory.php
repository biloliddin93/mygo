<?php

namespace Database\Factories;

use App\Models\Users_bank;
use Illuminate\Database\Eloquent\Factories\Factory;

class UsersBankFactory extends Factory
{
    protected $model = Users_bank::class;

    public function definition(): array
    {
    	return [
    	    //
    	];
    }
}
